import React, { Component } from 'react';
import { Header, Body } from "./components/template"

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    }
  }

  setIsLoggedIn = (status) => {
    this.setState({ isLoggedIn: status })
  }

  render() {
    const page = this.state.isLoggedIn
    return (
      <>
        <Header setIsLoggedIn={this.setIsLoggedIn} isLoggedIn={this.state.isLoggedIn} />
        { this.state.isLoggedIn === true ? <Body setIsLoggedIn={this.setIsLoggedIn} isLoggedIn={this.state.isLoggedIn} /> : "" }
      </>
    );
  }
}

// function App() {
//   return (
//     <div className="App">
//       <Header />
//       <Body />
//     </div>
//   );
// }

// export default App;
