import React, { Component } from 'react'

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    logout = () => {
        this.props.setIsLoggedIn(false);
    }
    
    render() {
        return (
            <div style={{
                width: "100%",
                height: "840px",
                backgroundColor: "#edf0f5",
                padding: "50px 0 "
            }}>
                <div style={{
                    margin: "0 auto",
                    width: "1700px",
                    paddingTop: "20px",
                    display: "flex"
                }}>
                    <div style={{
                        flex:1
                    }}>
                        <div style={{
                            width: "600px",
                            height: "auto",
                            fontSize: "24px",
                            fontWeight: "700",
                            lineHeight: "36px",
                            color: "#333",
                            fontFamily: "sans-serif",
                            textRendering: "optimizelegibility",
                        }}>
                            Facebook Helps you connect and share with the people in your life
                        </div>
                        <img src="connecting.png" style={{
                            width: "35%"
                        }}></img>
                        <div onClick={this.logout} style={{
                                      position: "relative",
                                      width: "50px",
                                      height: "20px",
                                      backgroundColor: "#5b72a9",
                                      border: "1px solid #999",
                                      borderColor: "#8b9dc3 #2f477a #29447e #1a356e",
                                      cursor: "pointer",
                                      fontSize: "12px",
                                      fontWeight: "bold",
                                      textAlign: "center",
                                      color: "#fff"
                                  }}>Logout</div>
                    </div>

                </div>

            </div>
        );
    }
}

export default Body