import React, { Component } from 'react'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    login = () => {
        this.props.setIsLoggedIn(true);
    }

    render() {
        return (
              <div style={{
                  width:"100%",
                  height:"90px",
                  display:"flex",
                  backgroundColor: "#3b5998"
              }}>
                  <div style={{
                      margin:"0 auto",
                      width:"1700px",
                      height:"10%"
                  }}>
                      <table style={{
                          float:"right",
                          marginTop:"15px"
                      }}>
                          <tr>
                              <td style={{
                                  color:"#fff",
                                  fontSize:"12px",
                                  width:"164px",
                                  cursor:"pointer"
                              }}>Email or Phone</td>
                              <td style={{
                                  color:"#fff",
                                  fontSize:"12px",
                                  width:"164px",
                                  cursor:"pointer"
                              }}>Password</td>
                          </tr>
                          <tr>
                              <td>
                                    <input type="text" style={{
                                        borderColor: "#1d2a5b",
                                        margin: 0,
                                        width: "142px",
                                        border: "1px solid #bdc7d8",
                                        padding: "3px",
                                        backgroundColor: "#ffffff"
                                    }}></input>
                              </td>
                              <td>
                                    <input type="password" style={{
                                        borderColor: "#1d2a5b",
                                        margin: 0,
                                        width: "142px",
                                        border: "1px solid #bdc7d8",
                                        padding: "3px",
                                        backgroundColor: "#ffffff"
                                    }}></input>
                              </td>
                              <td>
                                  <div onClick={this.login} style={{
                                      position: "relative",
                                      width: "50px",
                                      height: "20px",
                                      backgroundColor: "#5b72a9",
                                      border: "1px solid #999",
                                      borderColor: "#8b9dc3 #2f477a #29447e #1a356e",
                                      cursor: "pointer",
                                      fontSize: "12px",
                                      fontWeight: "bold",
                                      textAlign: "center",
                                      color: "#fff"
                                  }}>Log In</div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div></div>
                              </td>
                              <td style={{
                                  color: "#9daccb",
                                  fontSize: "11px",
                                  width: "164px",
                                  cursor: "pointer"
                              }}>
                                  Forgot your password?
                              </td>
                          </tr>
                      </table>
                      <h1 style={{
                          display:"flex",
                          padding:"19px 0",
                          fontSize:"40px",
                          color:"#fff",
                          margin:0,
                          letterSpacing:0.05
                      }}>facebook</h1>
                  </div>
              </div>
        );
    }
}

export default Header